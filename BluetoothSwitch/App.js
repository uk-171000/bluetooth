/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import {
  
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
} from 'react-native';

import {
  Header,
  LearnMoreLinks,
  Colors,
  DebugInstructions,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';

// Imported by me
import { BleManager } from 'react-native-ble-plx';


 class App extends React.Component {
  constructor() {
    super();
    this.manager = new BleManager();
    this.state = {  /**
      * The current state of the manager is unknown; an update is imminent.
      */
     Unknown: 'Unknown',
     /**
      * The connection with the system service was momentarily lost; an update is imminent.
      */
     Resetting: 'Resetting',
     /**
      * The platform does not support Bluetooth low energy.
      */
     Unsupported: 'Unsupported',
     /**
      * The app is not authorized to use Bluetooth low energy.
      */
     Unauthorized: 'Unauthorized',
     /**
      * Bluetooth is currently powered off.
      */
     PoweredOff: 'PoweredOff',
     /**
      * Bluetooth is currently powered on and available to use.
      */
     PoweredOn: 'PoweredOn'};
  }






componentWillMount(state) {
  console.log("Step 1 state: ", state);
  const subscription = this.manager.onStateChange((state) => {
      if (state === 'PoweredOn') {
        console.log("Step 2 state: ", state);
        this.scanAndConnect();
        subscription.remove();
        console.log("Step 3");
      }
  }, true);
}



scanAndConnect(state) {
  console.log("Step 4");


  this.manager.startDeviceScan(null, null, (error, device) => {
      if (error) {
          // Handle error (scanning will be stopped automatically)
          console.log("Step 5 state: ", state);
          return
      }
  
      console.log("Step 6");
  
      // Check if it is a device you are looking for based on advertisement data
      // or other criteria.
      //SmartCaravan_MI
      if (device.name === 'JBL TUNE205BT' || 
          device.name === 'C T 800E36___') {
    // Stop scanning as it's not necessary if you are scanning for one device.
            console.log("Step 7");
            console.log("device.name: ", device.name);
            this.manager.stopDeviceScan();
            console.log("Step 8");
    // Proceed with connection.


    device.connect()
    .then((device) => {
      console.log("Step 9");
        return device.discoverAllServicesAndCharacteristics()
    })
    .then((device) => {
      console.log("Step 10");
       // Do work on device with services and characteristics
    })
    .catch((error) => {
      console.log("Step 11");
        // Handle errors
    });


      }
  });
  
}



/*

  componentWillMount() {
    const subscription = this.manager.onStateChange((state) => {
        if (state === 'PoweredOn') {
            this.scanAndConnect();
            subscription.remove();
        }
    }, true);
}


scanAndConnect() {
  this.manager.startDeviceScan(null, null, (error, device) => {
      if (error) {
          // Handle error (scanning will be stopped automatically)
          return
      }

      // Check if it is a device you are looking for based on advertisement data
      // or other criteria.
      if (device.name === 'SmartCaravan' || 
          device.name === 'SensorTag') {
          

            this.info("Connecting to Tappy");

            console.log(device.name)


          // Stop scanning as it's not necessary if you are scanning for one device.
          this.manager.stopDeviceScan();

          // Proceed with connection.


          device.connect()
    .then((device) => {
        return device.discoverAllServicesAndCharacteristics()
    })
    .then((device) => {
       // Do work on device with services and characteristics
    })
    .catch((error) => {
        // Handle errors
    });


      }
  });
}
*/




render(){
  return (
    <>
      <StatusBar barStyle="dark-content" />
      <SafeAreaView>
        <ScrollView
          contentInsetAdjustmentBehavior="automatic"
          style={styles.scrollView}>
          <Header />
          {global.HermesInternal == null ? null : (
            <View style={styles.engine}>
              <Text style={styles.footer}>Engine: Hermes</Text>
            </View>
          )}
          <View style={styles.body}>
            <View style={styles.sectionContainer}>
              <Text style={styles.sectionTitle}>Step One Umang Kaswala 9</Text>
              <Text style={styles.sectionDescription}>
                                Edit <Text style={styles.highlight}>App.js</Text> to change this
                screen and then come back to see your edits.
              </Text>
            </View>
            <View style={styles.sectionContainer}>
              <Text style={styles.sectionTitle}>See Your Changes</Text>
              <Text style={styles.sectionDescription}>
                <ReloadInstructions />
              </Text>
            </View>
            <View style={styles.sectionContainer}>
              <Text style={styles.sectionTitle}>Debug</Text>
              <Text style={styles.sectionDescription}>
                <DebugInstructions />
              </Text>
            </View>
            <View style={styles.sectionContainer}>
              <Text style={styles.sectionTitle}>Learn More</Text>
              <Text style={styles.sectionDescription}>
                Read the docs to discover what to do next:
              </Text>
            </View>
            <LearnMoreLinks />
          </View>
        </ScrollView>
      </SafeAreaView>
    </>
  );}
}

const styles = StyleSheet.create({
  scrollView: {
    backgroundColor: Colors.lighter,
  },
  engine: {
    position: 'absolute',
    right: 0,
  },
  body: {
    backgroundColor: Colors.white,
  },
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: '600',
    color: Colors.black,
  },
  sectionDescription: {
    marginTop: 8,
    fontSize: 18,
    fontWeight: '400',
    color: Colors.dark,
  },
  highlight: {
    fontWeight: '700',
  },
  footer: {
    color: Colors.dark,
    fontSize: 12,
    fontWeight: '600',
    padding: 4,
    paddingRight: 12,
    textAlign: 'right',
  },
});

export default App;
